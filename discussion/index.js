// Operators
/*
	Add + = Sum

	Subtract - = Difference

	Multiply * = Product

	Divide / = Quotient

	Modulus % =  Remainder

	PMDAS 
		
*/


function mod() {
	return 9 % 2;
}

console.log(mod());

// Assignment operator (=)
/*
	+= (addition)
	-= (subtraction)
	*= (multiplication)
	/= (division)
	%= (modulo)
*/

	let x = 1;

	let sum = 1;
	//sum = sum + 1;
	sum += 1; 

	console.log(sum)


// Increment and Decrement (++, --)
// Operators that add or subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied to.

let z = 1;

let increment = ++z;

console.log("Result of pre-increment " + increment);
console.log("Result of pre-increment " + z);


// post-increment

increment = z++;
console.log("Result of post-increment: " + increment)
console.log("Result of post-increment: " + z)

let decrement = --z;

console.log("Result of pre-decrement " + decrement);
console.log("Result of pre-decrement " + z);


// post-decrement

decrement = z--;
console.log("Result of post-decrement: " + decrement)
console.log("Result of post-decrement: " + z)

//comparison operators

//equality operator (==)

let juan = "juan";

console.log(1 == 1);
console.log(0 ==  false);
console.log("juan" == juan)

// strict equality (===)
console.log(1 === true)

// inequality operator (!=) 
console.log("inequality operator")
console.log(1 != 1);
console.log("Juan" != juan)

//strict inequality (!==)
console.log(0 !== false);


//Other comparizon operators
/*
	> - Greater than
	< - Less than
	>= - greater than or equal

	<= - less than or equal
*/

// logical operators

let isLegalAge = true;
let isRegistered = false;

/*
	And operator (&&) - returns true if all operands are true

	true && true = true
	false && true = false
	true && false = false
	false && false = false

	Or operator (||) - returns true if at least on operands is true

	true && true = true
	false && true = true
	true && false = true
	false && false = false

*/

// And operator

let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of logical AND operator: " + allRequirementsMet);


//Or operator

let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of logical AND operator: " + someRequirementsMet);

// Selection control structures

/*
	IF statement
		-	execute a statement if a specified condition is true
		Syntax

		if(condition){
			statement/s;
		}
*/

let num = -1;


if (num < 0) {
	console.log("hello");
}

let value = 10;

if (value >= 10) {
	console.log("Welcome to Zuitt")
}


/*
	if-else statement
	- executes a statement if the previous conditions returns false.
		syntax:
		if(condition) {
			statement/s;
		} else {
			statements/s;
		}
*/

// num = 5;

// if (num >= 10) {
// 	console.log("Num is greater or equal to 10");
// } else {
// 	console.log("Num is not greater or equal to 10");
// }

// // 

// let age = parseInt(prompt("Please provide age: "));

// if (age > 59) {
// 	console.log("Senior Age");
// } else {
// 	console.log("invalid age");
// }

// if elseif else statement
/*

	Syntax:
	if(condition) {
	
	}
	else if (condition) {
	
	}

	.
	.
	.
	.

	else {
		statement
	}

*/

/*
let city = parseInt(prompt("Enter a number: "));

if (city === 1) {
	alert("Quezon");
}
else if (city === 2){
	alert("Valenzuela");
}
else if (city === 3){
	alert("Pasig");
}
else if (city ==== 4){
	alert("Taguig");
}
else {
	alert("invalid number");
}

*/


let message = "";

function determineTyphoonIntensity(windSpeed) {
	if (windSpeed < 30) {
		return "Not a typhoon yet";
	} 
	else if (windSpeed <= 61) {
		return "Tropical depression detected";
	}
	else if (windSpeed >= && windSpeed <= 88) {
		return "Tropical storm detected";
	}
	else if (windSpeed >= 89 && windSpeed <= 117) {
		return "Severe Tropical storm detected";
	}
	else {
		return "typhoon detected";
	}
}

message = determineTyphoonIntensity(70);

console.log(message);

// ternary operator
/*
	syntax:

	(condition ? ifTrue : ifFalse)
*/

/*let ternaryResult = (1 < 18) ? true : false;
console.log("Result of Ternary operator: " + ternaryResult);

function isOfLegalAge() {
	name = "John";
	return "You are of the legal age limit";
}

function isUnderAge() {
	name = "Jane";
	return "You are under the age limit";
}

let age = parseInt(prompt("What is your age: "));

let legalAge = (age >= 18) ? isOfLegalAge() : isUnderAge()
alert("Result of ternary operator in functions: " + legalAge, name)*/


// switch statement
/*
	syntax:

	switch (expression) {
		case value1:
			statement
			break;
		case value2: 
			statement
			break;
		case valueN:
			statement
			break;
		default:


	}
*/

let day = prompt("What day of the week is it today?").toLowerCase();

switch (day) {
	case "sunday":
		alert("Thecolor of the day is red");
		break;
	case "monday":
		alert("Thecolor of the day is orange");
		break;
	case "tuesday":
		alert("Thecolor of the day is yellow");
		break;
	case "wednesday":
		alert("Thecolor of the day is green");
		break;
	case "thursday":
		alert("Thecolor of the day is blue");
		break;
	case "friday":
		alert("Thecolor of the day is indigo");
		break;
	case "saturday":
		alert("Thecolor of the day is violet");
		break;
	default:
		alert("Please input valid day")
}


// try catch finally statement - commonly used for error handling

function showIntesityAlert(windSpeed) {
	try {
		alerat(determineTyphoonIntensity(windSpeed));
	}
	catch(error) {
		console.log(typeof error)
	}
	finally{
		alert("Intensity updates will show new alert")
	}
}