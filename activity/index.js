console.log("Hello World")

let number1 = prompt("Provide a number");
let number2 = prompt("Provide another number");
let sum = number1 + number2;
let answer = 0;

if (sum < 10) {
	answer = number1 + number2;
}
else if (sum >= 10 || sum <= 20) {
	answer = number1 - number2;
}
else if (sum >= 21 || sum <= 30) {
	answer = number1 * number2;
}
else if (sum >= 31) {
	answer = number1 / number2;
}

if (answer >= 10) {
	alert("The total is: " + answer);
} else {
	console.warn("The total is: " + answer);
}

// 

let name = prompt("Please enter your name: ");
let age = prompt("Please enter your age: ");

if (name === "" || name === null || age === "" || age === null) {
	alert("Are you a time traveler?");
} 
else if ((name !== "" && name !== null) && (age !== "" && age !== null)) {
	alert (`Hello ${name}. Your age is ${age}`);
}

function isLegalAge(age) {
	if (age >= 18) {
		alert("You are of legal age.");
	}
	else if (age <= 17) {
		alert("You are not allowed here");
	}
}

//

isLegalAge(age);

switch (age) {
	case "18": 
		alert("You are now allowed to party");
		break;
	case "21": 
		alert("You are now part of the adult society");
		break;
	case "65":
		alert("We thank you for your contribution to society");
		break;
	default:
		alert("Are you sure you're not an alien?");
}

//

try {
	isLegalag(age);
}
catch (error) {
	console.warn(typeof error)
}
finally {
	isLegalAge(age);
}